FROM amazon/aws-cli

RUN yum install -y jq zip 

# https://immich.app/docs/administration/backup-and-restore/

COPY entrypoint.sh /entrypoint.sh

VOLUME [ "/backup/db" ] # db dump 
VOLUME [ "/backup/immich" ] # immich folder dump
VOLUME [ "/tmp" ] # temp location for zip files

ENTRYPOINT ["/entrypoint.sh"]
