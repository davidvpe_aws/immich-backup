#!/bin/sh

log() {
  echo "$(date -u +"%Y-%m-%dT%H:%M:%SZ") - $1"
}

for var in S3_BUCKET INTERVAL_SECONDS AWS_DEFAULT_REGION AWS_ACCESS_KEY_ID AWS_SECRET_ACCESS_KEY; do
  if [ -z "$(eval echo \$$var)" ]; then
    log "$var is not set. Exiting."
    exit 1
  fi
done

while true; do
  current_hour=$(date +%H)
  current_minute=$(date +%M)
  current_second=$(date +%S)
  current_time_in_seconds=$((10#$current_hour*3600 + 10#$current_minute*60 + 10#$current_second))
  target_time_in_seconds=$((3*3600)) # 3 AM
  sleep_seconds=$((target_time_in_seconds - current_time_in_seconds))

  if [ $sleep_seconds -lt 0 ]; then
    sleep_seconds=$((86400 + sleep_seconds)) # add 24 hours if it's past 3 AM
  fi

  sleep $sleep_seconds

  # log "Starting backup"
  # log "Cleaning up /tmp/"
  # rm -rf /tmp/*

  # log "Zipping folder for lower object count"
  # zip -r /tmp/immich_folder.zip /backup/immich
  # log "Zipping db for lower object count"
  # zip -r /tmp/immich_db.zip /backup/db

  log "Syncing /backup to s3://$S3_BUCKET"
  
  aws s3 sync /backup s3://$S3_BUCKET

  hours=$((sleep_seconds / 3600))
  minutes=$(((sleep_seconds % 3600) / 60))
  seconds=$((sleep_seconds % 60))

  time_string=""
  [[ $hours -gt 0 ]] && time_string="${hours}h "
  [[ $minutes -gt 0 ]] && time_string+="${minutes}m "
  [[ $seconds -gt 0 ]] && time_string+="${seconds}s"

  log "Sleeping for $time_string"

  sleep $sleep_seconds
done

