import * as cdk from "aws-cdk-lib";
import { AccountPrincipal, Effect, PolicyStatement } from "aws-cdk-lib/aws-iam";
import { Construct } from "constructs";
// import * as sqs from 'aws-cdk-lib/aws-sqs';

export class ImmichBackupStack extends cdk.Stack {
  constructor(scope: Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    const backupBucket = new cdk.aws_s3.Bucket(this, "BackupBucket", {
      bucketName: `immich-backup-bucket-${cdk.Aws.ACCOUNT_ID}-${cdk.Aws.REGION}`,
      removalPolicy: cdk.RemovalPolicy.RETAIN,
      blockPublicAccess: {
        blockPublicAcls: true,
        blockPublicPolicy: true,
        ignorePublicAcls: true,
        restrictPublicBuckets: true,
      },
      publicReadAccess: false,
      versioned: true,
      lifecycleRules: [
        {
          enabled: true,
          transitions: [
            {
              storageClass: cdk.aws_s3.StorageClass.DEEP_ARCHIVE,
              transitionAfter: cdk.Duration.days(5),
            },
          ],
        },
        {
          enabled: true,
          noncurrentVersionExpiration: cdk.Duration.days(365),
        },
      ],
    });

    backupBucket.addToResourcePolicy(
      new PolicyStatement({
        actions: ["s3:*"],
        resources: [backupBucket.bucketArn, `${backupBucket.bucketArn}/*`],
        principals: [new AccountPrincipal(cdk.Aws.ACCOUNT_ID)],
        effect: Effect.ALLOW,
      })
    );

    new cdk.CfnOutput(this, "BackupBucketOutput", {
      value: backupBucket.bucketName,
    });
  }
}
